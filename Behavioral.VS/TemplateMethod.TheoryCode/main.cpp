#include "template_method.hpp"

using namespace std;

void client_function(AbstractClass& ac)
{
	ac.template_method();
}

int main()
{
	ConcreteClassA ca;
	ConcreteClassB cb;

	client_function(ca);

	cout << "\n\n";

	client_function(cb);

	system("PAUSE");
}
