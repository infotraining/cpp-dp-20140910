#ifndef TEMPLATE_METHOD_HPP_
#define TEMPLATE_METHOD_HPP_

#include <iostream>
#include <string>
#include <memory>

class Service
{
public:
	virtual void run()
	{
		std::cout << "Service::run()" << std::endl;
	}

	virtual ~Service()
	{}
};

class OtherService : public Service
{
public:
	virtual void run() override
	{
		std::cout << "OtherService::run()" << std::endl;
	}
};

// "AbstractClass"
class AbstractClass
{
protected:
	virtual bool is_valid()
	{
		return true;
	}
	
	virtual void primitive_operation_1() = 0;
	virtual void primitive_operation_2() = 0;
	
	virtual void log_error(const std::string& error_msg)
	{}
	
	virtual std::unique_ptr<Service> create_service()
	{
		return std::unique_ptr<Service>(new Service());
	}
public:
	void template_method()
	{
		std::unique_ptr<Service> srv = create_service();
		srv->run();
		primitive_operation_1();
		if (is_valid())
			primitive_operation_2();
		else
			log_error("Invalid state");
	}
	
	virtual ~AbstractClass() {}
};

// "ConcreteClass"
class ConcreteClassA : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation2()" << std::endl;
	}
};

// "ConcreteClass"
class ConcreteClassB : public AbstractClass
{
protected:
	bool is_valid() override
	{
		return false;
	}

	void log_error(const std::string& error_msg) override
	{
		std::cout << "LOG: " << error_msg << std::endl;
	}

	void primitive_operation_1()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation2()" << std::endl;
	}

	std::unique_ptr<Service> create_service() override
	{
		return std::unique_ptr<Service>(new OtherService);
	}
};

#endif /*TEMPLATE_METHOD_HPP_*/
