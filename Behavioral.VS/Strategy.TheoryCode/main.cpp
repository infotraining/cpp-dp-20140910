#include "strategy.hpp"

using namespace std;

int main()
{
	shared_ptr<Strategy> s1 = make_shared<ConcreteStrategyA>();
	shared_ptr<Strategy> s2 = make_shared<ConcreteStrategyB>();
	shared_ptr<Strategy> s3 = make_shared<ConcreteStrategyC>();

	Context context(s1);

	context.context_interface();

	context.reset_strategy(s2);

	context.context_interface();

	context.reset_strategy(s3);

	context.context_interface();

	system("PAUSE");
}
