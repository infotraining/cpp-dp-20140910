#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>

struct AccountContext
{
	double balance;
	const int id;

	AccountContext(int id, double balance = 0.0) : id(id), balance(balance)
	{}
};

class AccountState
{
	double interest_rate_;
public:
	AccountState(double interest_rate) : interest_rate_(interest_rate)
	{
	}

	virtual AccountState* withdraw(AccountContext& context, double amount) = 0;

	virtual AccountState* deposit(AccountContext& context, double amount)
	{
		context.balance += amount;

		if (context.balance >= 0)
			return AccountState::Normal;
		else
			return AccountState::Overdraft;
	}

	virtual AccountState* pay_interest(AccountContext& context)
	{
		context.balance += context.balance * interest_rate_;

		return this;
	}

	virtual std::string status() const = 0;

	virtual ~AccountState() {}

	static AccountState* Normal;
	static AccountState* Overdraft;
};

class Normal : public AccountState
{
public:
	Normal(double interest_rate = 0.15) : AccountState(interest_rate) {}
	virtual AccountState* withdraw(AccountContext& context, double amount)
	{
		context.balance -= amount;

		if (context.balance < 0)
			return AccountState::Overdraft;
		else
			return this;
	}

	virtual std::string status() const
	{
		return "Normal";
	}
};

class Overdraft : public AccountState
{
public:
	Overdraft(double interest_rate = 0.3) : AccountState(interest_rate) {}
	
	virtual AccountState* withdraw(AccountContext& context, double amount)
	{
		std::cout << "Brak srodkow na koncie #" << context.id << std::endl;

		return this;
	}

	virtual std::string status() const
	{
		return "Overdraft";
	}

};

class BankAccount
{
	AccountContext context_;
	AccountState* state_;
public:
	BankAccount(int id) : context_(id), state_(AccountState::Normal) {}

	void withdraw(double amount)
	{
		assert(amount > 0);

		state_ = state_->withdraw(context_, amount);
	}

	void deposit(double amount)
	{
		assert(amount > 0);

		state_ = state_->deposit(context_, amount);
	}

	void pay_interest()
	{
		state_->pay_interest(context_);
	}

	void print_status() const
	{
		std::cout << "BankAccount #" << context_.id
			<< "; State: " << state_->status()
			<< "; Balance: " << context_.balance << std::endl;
	}

	double balance() const
	{
		return context_.balance;
	}

	int id() const
	{
		return context_.id;
	}
};

#endif
