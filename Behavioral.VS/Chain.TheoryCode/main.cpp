#include <iostream>
#include "chain.hpp"

using namespace std;

int main()
{
	// Setup Chain of Responsibility
	Handler* h1 = new ConcreteHandler1();
	Handler* h2 = new ConcreteHandler2();
	Handler* h3 = new ConcreteHandler3();
	Handler* h4 = new EvenHandler();
	Handler* h5 = new OddHandler();
	h1->set_successor(h2);
	h2->set_successor(h3);
	h3->set_successor(h4);
	h4->set_successor(h5);


	// Generate and process request
	int requests[8] = {2, 5, 14, 222, 18, 3, 27, 20};

	for(int* p = requests; p != requests + 8; ++p)
    {
		h1->handle_request(*p);
    }

	delete h3;
	delete h2;
	delete h1;

	system("PAUSE");
}
