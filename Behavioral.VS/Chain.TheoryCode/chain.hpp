#ifndef CHAIN_HPP_
#define CHAIN_HPP_

#include <iostream>
#include <string>

// "Handler"
class Handler
{
protected:
	Handler* successor_;

	virtual bool is_match(int request) const = 0;
	virtual void do_handle_request(int request, bool& accepted) = 0;
public:
	Handler() : successor_(0) {}

	void set_successor(Handler* successor)
	{
		successor_ = successor;
	}

	void handle_request(int request)
	{
		bool is_accepted = false;

		if (is_match(request))
			do_handle_request(request, is_accepted);
		
		if (successor_ != NULL && !is_accepted)
			successor_->handle_request(request);
	}

	virtual ~Handler() {}
};

// "ConcreteHandler1"
class ConcreteHandler1 : public Handler
{
protected:
	bool is_match(int request) const
	{
		return (request >= 0) && (request < 10);
	}

	void do_handle_request(int request, bool& accepted)
	{
		std::cout << "ConcreteHandler1 handled request " << request << std::endl;
	}
};

// "ConcreteHandler2"
class ConcreteHandler2 : public Handler
{
protected:
	bool is_match(int request) const
	{
		return (request >= 10) && (request < 20);
	}

	void do_handle_request(int request, bool& accepted)
	{
		std::cout << "ConcreteHandler2 handled request " << request << std::endl;
		std::cout << "Message with " << request << " is accepted" << std::endl;
		accepted = true;
	}
};

// "ConcreteHandler3"
class ConcreteHandler3 : public Handler
{
protected:
	bool is_match(int request) const
	{
		return (request >= 20) && (request < 30);
	}

	void do_handle_request(int request, bool& accepted)
	{
		std::cout << "ConcreteHandler3 handled request " << request << std::endl;
	}
};

class EvenHandler : public Handler
{
public:
	virtual bool is_match(int request) const
	{
		return request % 2 == 0;
	}

	virtual void do_handle_request(int request, bool& accepted)
	{
		std::cout << "Is Even: " << request << std::endl;
	}
};

class OddHandler : public Handler
{
public:
	virtual bool is_match(int request) const
	{
		return request % 2;
	}

	virtual void do_handle_request(int request, bool& accepted)
	{
		std::cout << "Is Odd: " << request << std::endl;
	}
};

#endif /*CHAIN_HPP_*/
