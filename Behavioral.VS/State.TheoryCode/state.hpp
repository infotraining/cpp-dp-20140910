#ifndef STATE_HPP_
#define STATE_HPP_

#include <iostream>
#include <string>
#include <typeinfo>
#include <memory>


class Context;

// "State"
class State
{
public:
	virtual State* handle(Context* context) = 0;
	virtual ~State() {}
};

// "ConcreteStateA"
class ConcreteStateA : public State
{
public:
	State* handle(Context* context);
};


// "ConcreteStateB"
class ConcreteStateB : public State
{
public:
	State* handle(Context* context);
};

// "Context"
class Context
{
	std::unique_ptr<State> state_;

	Context(const Context&);
	Context& operator=(const Context&);
public:
	Context(State* state) : state_(state)
	{
	}

	void request()
	{
		state_.reset(state_->handle(this));

		std::cout << "State: " << typeid(*state_).name() << std::endl;
	}
};

State* ConcreteStateA::handle(Context* context)
{
	return new ConcreteStateB();
}

State* ConcreteStateB::handle(Context* context)
{
	return new ConcreteStateA();
}

#endif /*STATE_HPP_*/
