#include <iostream>
#include <vector>
#include "abstract_factory.hpp"

using namespace std;

class Vector
{
	Vector(const Vector& v); // copy consstructor
	Vector(Vector&& v); // move constructor
};

vector<int> create_table()
{
	vector<int> result;

	for (int i = 0; i < 10000; ++i)
		result.push_back(i);

	return result;
}

int main()
{
	vector<int> vec1 = create_table();
	vector<int> vec2 = move(vec1); // kopia

	string word = "Hello";

	vector<string> words;

	words.push_back(string("Hello"));

	cout << "word after move: " << word << endl;

	cout << "words[0]: " << words[0] << endl;

	// Abstract factory #1
	ConcreteFactory1 factory1;
	Client client1(factory1);
	client1.Run();


	// Abstract factory #2
	ConcreteFactory2 factory2;
	Client client2(factory2);
	client2.Run();

	system("PAUSE");
}
