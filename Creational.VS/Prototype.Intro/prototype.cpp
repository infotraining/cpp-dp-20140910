#include <iostream>
#include <cassert>
#include <typeinfo>

using namespace std;

class Engine
{
public:
	virtual void start() = 0;
	virtual void stop() = 0;

	Engine* clone() const
	{
		// 1 - klonowanie
		Engine* cloned_engine = do_clone();
		
		// 2 - sprwadzenie typu
		assert(typeid(*cloned_engine) == typeid(*this));

		return cloned_engine;
	}

	virtual ~Engine() {}
protected:
	virtual Engine* do_clone() const = 0;
};

template <typename EngineType, typename BaseEngine = Engine>
class CloneableEngine : public BaseEngine
{
protected:
	virtual Engine* do_clone() const override
	{
		return new EngineType(*static_cast<const EngineType*>(this));
	}
};

// CRTP
class Diesel : public CloneableEngine<Diesel>
{
public:
	virtual void start()
	{
		cout << "Diesel starts\n";
	}

	virtual void stop()
	{
		cout << "Diesel stops\n";
	}
};

class TDI : public CloneableEngine<TDI, Diesel>
{
public:
	virtual void start()
	{
		cout << "TDI starts\n";
	}

	virtual void stop()
	{
		cout << "TDI stops\n";
	}
};

class Hybrid : public CloneableEngine<Hybrid>
{
public:
	virtual void start()
	{
		cout << "Hybrid starts\n";
	}

	virtual void stop()
	{
		cout << "Hybrid stops\n";
	}
protected:
	virtual Hybrid* do_clone() const
	{
		return new Hybrid(*this);
	}
};

class Car
{
	Engine* engine_;
public:
	Car(Engine* engine) : engine_(engine)
	{}

	Car(const Car& source) : engine_(source.engine_->clone())
	{
	}

	~Car() { delete engine_; }

	void drive(int km)
	{
		engine_->start();
		cout << "Driving " << km << " kms\n";
		engine_->stop();
	}
};

int main()
{
	Car c1(new TDI());

	c1.drive(100);

	cout << "\n";

	Car c2 = c1;

	c2.drive(200);

	system("PAUSE");
}

