#ifndef MULTITHREADING_SINGLETON_HPP
#define MULTITHREADING_SINGLETON_HPP

//#include <boost/thread.hpp>
#include <thread>
#include <boost/noncopyable.hpp>

namespace Multithreading
{

	template<typename T>
	class SingletonHolder //: boost::noncopyable
	{
	private:
		SingletonHolder();
		static T* instance_;
		static std::once_flag init_flag_;
		static void init_instance()
		{
			instance_ = new T();
		}
	public:
		SingletonHolder(const SingletonHolder&) = delete;
		SingletonHolder& operator=(const SingletonHolder&) = delete;

		static T& instance()
		{
			std::call_once(init_flag_, &SingletonHolder<T>::init_instance);
			return *instance_;
		}
	};

	template<typename T>
	T* SingletonHolder<T>::instance_;

	template<typename T>	
	std::once_flag SingletonHolder<T>::init_flag_;
}

#endif