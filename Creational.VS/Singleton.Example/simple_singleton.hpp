#ifndef SIMPLE_SINGLETON_HPP_
#define SIMPLE_SINGLETON_HPP_

#include <iostream>
#include <mutex>
#include <atomic>

namespace Simple
{

// leniwa wersja z operatorem new ---------------------------------------------
template <typename T>
class SingletonHolder
{
public:
	static T& instance()
	{
		// double checked locking pattern
		T* temp = instance_.load();
		if (!temp)
		{
			std::lock_guard<std::mutex> lk(mtx_);

			temp = instance_.load();
			if (!temp)
			{
				temp = new T();
				instance_.store(temp);
			}
		}

		return *instance_;
	}

private:
	SingletonHolder();
	SingletonHolder(const SingletonHolder&);
	SingletonHolder& operator=(const SingletonHolder&);

	static std::mutex mtx_;
	static std::atomic<T*> instance_;
};

template <typename T>
T* SingletonHolder<T>::instance_ = 0;

}
#endif /* SIMPLE_SINGLETON_HPP_ */
