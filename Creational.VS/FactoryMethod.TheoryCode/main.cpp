#include <iostream>
#include <cstdlib>
#include <map>
#include <vector>
#include <string>

#include "factory.hpp"

using namespace std;

class Client
{
private:
	vector<Product*> products_;
	Creator* creator_;
public:
	Client(Creator* creator) : creator_(creator)
	{
	}

	Client(const Client&);
	Client& operator=(const Client&);

	~Client()
	{
		auto it = products_.begin();
		for( ; it != products_.end(); ++it)
			delete *it;
	}

	void init(size_t size)
	{
		for(size_t i = 0; i < size; ++i)
			products_.push_back(creator_->factory_method());
	}

	void use()
	{
		cout << "Content of client:\n";
		vector<Product*>::const_iterator it = products_.begin();
		for( ; it != products_.end(); ++it)
			cout << (*it)->description() << endl;
	}
};

int main()
{
	std::map<std::string, Creator*> creators;
	creators.insert(make_pair("ProductA", new ConcreteCreatorA()));
	creators.insert(make_pair("ProductB", new ConcreteCreatorB()));
	creators.insert(make_pair("ProductC", new ConcreteCreatorC()));

	Client client(creators["ProductC"]);
	client.init(10);
	client.use();

	// sprzątanie
	for(auto it = creators.begin(); it != creators.end(); ++it)
		delete it->second;

	system("PAUSE");
}
