#include "square.hpp"
#include "shape_factory.hpp"

using namespace Drawing;

static bool is_registered 
		= ShapeFactory::instance().register_creator("Square", ShapeCreator<Square>());
