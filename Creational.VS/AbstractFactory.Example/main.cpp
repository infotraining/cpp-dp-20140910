#include "game.hpp"

using namespace Game;

int main()
{
	GameApp game;

	GameLevel level = EASY;

	game.select_level(level);
	game.init_game(12);
	game.test_monsters();

	system("PAUSE");
}
