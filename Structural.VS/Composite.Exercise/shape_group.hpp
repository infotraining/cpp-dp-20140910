#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <list>
#include <memory>

namespace Drawing
{

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
	class ShapeGroup : public Shape
	{
		typedef std::shared_ptr<Shape> ShapePtr;
		std::list<ShapePtr> shapes_;
	public:
		ShapeGroup() {}

		ShapeGroup(const ShapeGroup& source)
		{
			for (auto shape : source.shapes_)
			{
				shapes_.emplace_back(shape->clone());
			}
		}

		virtual void draw() const
		{
			for (auto shape : shapes_)
				shape->draw();
		}

		virtual void move(int dx, int dy)
		{
			for (auto shape : shapes_)
				shape->move(dx, dy);
		}

		virtual void read(std::istream& in)
		{
			int count;
			in >> count;

			for (int i = 0; i < count; ++i)
			{
				std::string type_identifier;
				in >> type_identifier;

				Shape* shape = ShapeFactory::instance().create(type_identifier);
				shape->read(in);

				add(shape);
			}
		}

		virtual void write(std::ostream& out)
		{
			out << "ShapeGroup " << shapes_.size() << std::endl;
			for (auto shape : shapes_)
				shape->write(out);
		}

		virtual Shape* clone() const
		{
			return new ShapeGroup(*this);
		}

		void add(Shape* shape)
		{
			shapes_.emplace_back(shape);
		}
	};
}

#endif /*SHAPE_GROUP_HPP_*/
