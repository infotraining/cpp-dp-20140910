#include "bridge.hpp"
#include <vector>

struct Image::Impl
{
	std::vector<char> buffer_;

	Impl(size_t size) : buffer_(size)
	{}
};

Image::Image() : impl_(new Image::Impl(1024))
{

}

void Image::draw() const
{
	for (size_t i = 0; i < impl_->buffer_.size(); ++i)
		std::cout << impl_->buffer_[i];
}




