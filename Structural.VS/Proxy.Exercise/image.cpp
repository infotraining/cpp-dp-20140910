#include "image.hpp"
#include "clone_factory.hpp"

using namespace std;

namespace
{
	bool is_registered = 
		Drawing::ShapeFactory::instance().register_shape("Image", 
			new Drawing::ImageProxy());
}

void Drawing::ImageProxy::move(int dx, int dy)
{
	ShapeBase::move(dx, dy);

	if (img_)
		img_->move(dx, dy);
}

void Drawing::ImageProxy::draw() const
{
	if (!img_)
	{
		img_.reset(new Image(point().x(), point().y(), path_));
	}

	img_->draw();
}

void Drawing::ImageProxy::read(std::istream& in)
{
	Point pt;
	std::string path;

	in >> pt >> path;

	set_point(pt);
	set_path(path);
}

void Drawing::ImageProxy::write(std::ostream& out)
{
	out << "Image " << point() << " " << path_ << endl;
}

Drawing::Shape* Drawing::ImageProxy::clone() const
{
	return new ImageProxy(*this);
}

void Drawing::ImageProxy::set_path(std::string path)
{
	path_ = path;
}
