#ifndef TEXT_HPP
#define TEXT_HPP

#include <string>
#include "shape.hpp"
#include "paragraph.hpp"

namespace Drawing
{

// TODO - zaadaptowaĉ klase Paragraph do wymogow klienta
class Text : public ShapeBase, private LegacyCode::Paragraph
{
public:
	Text(int x = 0, int y = 0, const std::string& text = "")
		: ShapeBase(x, y), LegacyCode::Paragraph(text.c_str())
	{}

	virtual void draw() const;

	virtual void read(std::istream& in);

	virtual void write(std::ostream& out);

	virtual Shape* clone() const;
};

}

#endif