#include "text.hpp"
#include "clone_factory.hpp"

using namespace std;

namespace
{
	using namespace Drawing;

	// TODO - rejestracja prototypu Text
	bool is_registered = ShapeFactory::instance().register_shape("Text", new Text());
}

void Drawing::Text::draw() const
{
	render_at(point().x(), point().y());
}

void Drawing::Text::read(std::istream& in)
{
	Point pt;
	string text;

	in >> pt >> text;

	set_point(pt);
	set_paragraph(text.c_str());
}

void Drawing::Text::write(std::ostream& out)
{
	out << "Text " << point() << " " << get_paragraph() << endl;
}

Drawing::Shape* Drawing::Text::clone() const
{
	return new Text(*this);
}
