#include "coffeehell.hpp"
#include <memory>

class CoffeeBuilder
{
	std::unique_ptr<Coffee> coffee_;
public:
	template <typename Base>
	CoffeeBuilder& create_base()
	{
		coffee_ = std::unique_ptr<Coffee>(new Base);

		return *this;
	}

	template <typename Condiment>
	CoffeeBuilder& add()
	{
		if (!coffee_)
			throw std::logic_error("Base is empty");

		Coffee* temp = new Condiment(coffee_.release());
		coffee_.reset(temp);

		return *this;
	}

	std::unique_ptr<Coffee> get_coffee()
	{
		return move(coffee_);
	}
};

int main()
{
	//Coffee* cf = new ExtraEspresso(
	//				new Whipped(
	//					new Whisky(
	//						new Whisky(
	//							new Whisky(
	//								new Whisky(
	//									new Espresso()))))));

	CoffeeBuilder bld;
	bld.create_base<Espresso>();
	bld.add<Whisky>().add<Whisky>().add<Whipped>();

	std::unique_ptr<Coffee> cf = bld.get_coffee();


	std::cout << "Description: " << cf->get_description() << "; Price: " << cf->get_total_price() << std::endl;
	cf->prepare();

	system("PAUSE");
}
